# Code and data for "Hippocampal Subfield Volumes in Abstinent Men and Women with a History of Alcoholism"

This repository contains the code and preprocessed data used to generate tables and figures for "Alcoholism Gender Differences in Brain Responsivity to Emotional Stimuli", by Kayle S. Sawyer, Ph.D., Daniel M. Salz, Ph.D., Noor Adra, B.A., Maaria Kemppainen, M.A., Susan M. Ruiz, Ph.D., Gordon J. Harris, Ph.D., and Marlene Oscar-Berman, Ph.D., DOI: 

The code in this repository was written in 2019 and 2020 by 
Kayle S. Sawyer (email: kslays@bu.edu), Daniel M. Salz, and Noor Adra.
To the extent possible under law, the authors have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. The software is dedicated with the Creative Commons CC0 1.0 Universal Public Domain Dedication. This software is distributed without any warranty.
A copy of the CC0 Public Domain Dedication is included in this repository ("LICENSE.txt"). See <http://creativecommons.org/publicdomain/zero/1.0/>.

The R project ("moblab-hippocampus.Rproj") was written to work with RStudio version 1.1.453. The working directory should be set to the directory that contains this file. Opening the R project file in RStudio will do that automatically.

The code ("code/moblab-hippocampus.R") was written to work with R version 3.5.0.

The preprocessed data ("R_data/moblab-hippocampus.csv") are in comma separated value format. The columns represent various features, including demographic attributes and brain volumes, as described in the methods of the associated manuscript, available at the DOI: https://doi.org/10.1101/715292

Descriptions of the variables are as follows.

id: participant number

group: alcohol group assignment (levels are ‘Alcoholic’ or ‘Nonalcoholic Control’)

gender: gender presentation (levels are ‘Men’ or ‘Women’)

confounding: subsample assignment, as described in the manuscript methods (levels are 'confounded' or 'unconfounded')

age: rounded to nearest year, in years

education: education, in years

IMI: Immediate Memory Index from Wechsler Memory Scale-IV

DMI: Delayed (General) Memory Index from Wechsler Memory Scale-IV	

FS: Full Scale Intelligence Quotient from Wechsler Adult Intelligence Scale-IV

VC: Verbal Comprehension Index from Wechsler Adult Intelligence Scale-IV

PR: Perceptual Reasoning Index from Wechsler Adult Intelligence Scale-IV

WM: Working Memory Index from Wechsler Adult Intelligence Scale-IV

PS: Processing Speed Index from Wechsler Adult Intelligence Scale-IV

GA: General Ability Index from Wechsler Adult Intelligence Scale-IV

DHD: Duration of Heavy Drinking (21 or more drinks per week), in years

DD: Daily Drinks, in ounces of ethanol per day

LOS: Length of Sobriety, in years

cig: number of cigarettes smoked per day

drug5year: frequency of drug use within the past five years (levels are 'No', 'less than once per week', or 'once per week or more')

druglifetime: frequency of livetime drug use (levels are 'No', 'less than once per week', or 'once per week or more')

alcmother: history of drug and alcohol use of mother (levels are 'Alcoholic', 'Alcoholic and Drug Abuse', 'Drug Abuse', 'Never', 'Social Drinker', or 'Don't Know')

alcfather: history of drug and alcohol use of mother (levels are 'Alcoholic', 'Alcoholic and Drug Abuse', 'Drug Abuse', 'Never', 'Social Drinker', or 'Don't Know')

alc1deg: First degree alcohol history was indicated by participant endorsement of ‘Alcoholic’ for mother, father, sibling, or offspring (levels are 'Yes' or 'No')

alc2deg: Second degree alcohol history was indicated by participant endorsement of ‘Alcoholic’ for grandparents, aunts, uncles, or grandchildren (levels are 'Yes' or 'No')

eTIV: estimated total intracranial volume, in cubic millimeters

left_Hippocampal_tail: left hippocampal tail subfield, in cubic millimeters

left_subiculum: left subiculum subfield, in cubic millimeters

left_CA1: left cornu ammonis area 1 subfield, in cubic millimeters

left_Hippocampal-fissure: left hippocampal fissure subfield, in cubic millimeters

left_presubiculum: left presubiculum subfield, in cubic millimeters

left_parasubiculum: left parasubiculum subfield, in cubic millimeters

left_molecular_layer_HP: left molecular layer of the cornu ammonis fields 1-4 and subiculum subfield, in cubic millimeters

left_GC-ML-DG: left Granular Cell layer and Molecular Layer of the Dentate Gyrus, in cubic millimeters

left_CA3: left cornu ammonis area 2 and 3 subfield, in cubic millimeters

left_CA4: left cornu ammonis area 4 subfield, in cubic millimeters

left_fimbria: left fimbria subfield, in cubic millimeters 

left_HATA: left hippocampal amygdala transition area subfield, in cubic millimeters

right_Whole_hippocampus: right all subfields combined, in cubic millimeters

right_Hippocampal_tail: right hippocampal tail subfield, in cubic millimeters

right_subiculum: right subiculum subfield, in cubic millimeters

right_CA1: right cornu ammonis area 1 subfield, in cubic millimeters

right_Hippocampal-fissure: right hippocampal fissure subfield, in cubic millimeters

right_presubiculum: right presubiculum subfield, in cubic millimeters

right_parasubiculum: right parasubiculum subfield, in cubic millimeters

right_molecular_layer_HP: right molecular layer of the cornu ammonis fields 1-4 and subiculum subfield, in cubic millimeters

right_GC-ML-DG: right Granular Cell layer and Molecular Layer of the Dentate Gyrus

right_CA3: right cornu ammonis area 2 and 3 subfield, in cubic millimeters

right_CA4: right cornu ammonis area 4 subfield, in cubic millimeters

right_fimbria: right fimbria subfield, in cubic millimeters 

right_HATA: right hippocampal amygdala transition area, in cubic millimeters

right_Whole_hippocampus: right all subfields combined, in cubic millimeters
